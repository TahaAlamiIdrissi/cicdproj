package fr.tse.ensias.cicdproj.repository;

import fr.tse.ensias.cicdproj.domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepo extends JpaRepository<Product,Long> {
}
