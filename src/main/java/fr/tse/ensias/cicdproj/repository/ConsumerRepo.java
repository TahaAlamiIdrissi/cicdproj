package fr.tse.ensias.cicdproj.repository;

import fr.tse.ensias.cicdproj.domain.Consumer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConsumerRepo extends JpaRepository<Consumer,Long> {
}
