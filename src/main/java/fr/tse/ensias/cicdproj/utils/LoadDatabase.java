package fr.tse.ensias.cicdproj.utils;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class LoadDatabase {

    @Bean
    @Profile("test")
    CommandLineRunner initDatabase(){
        return args -> {

        };
    }

}
