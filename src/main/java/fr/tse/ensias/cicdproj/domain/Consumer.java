package fr.tse.ensias.cicdproj.domain;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.*;


@Entity
public class Consumer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String username;
    private String email;
    private Instant createdAt;
    @ManyToMany(mappedBy = "clients")
    private Collection<Product> products;

    public Consumer() {
    }

    public Consumer(Long id, String username, String email) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.products = new ArrayList<>();
        this.createdAt = Instant.now();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public Collection<Product> getProducts() {
        return products;
    }

    public void setProducts(Collection<Product> products) {
        this.products = products;
    }
}
