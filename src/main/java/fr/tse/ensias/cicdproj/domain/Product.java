package fr.tse.ensias.cicdproj.domain;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private double price;
    private long qte;
    private Instant createdAt;
    @ManyToMany
    private Collection<Consumer> clients;

    public Product() {
    }

    public Product(Long id, String name, double price, long qte) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.qte = qte;
        this.clients = new ArrayList<>();
        this.createdAt = Instant.now();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public long getQte() {
        return qte;
    }

    public void setQte(long qte) {
        this.qte = qte;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public Collection<Consumer> getClients() {
        return clients;
    }

    public void setClients(Collection<Consumer> clients) {
        this.clients = clients;
    }
}
