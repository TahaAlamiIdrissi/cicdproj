package fr.tse.ensias.cicdproj.service.impl;

import fr.tse.ensias.cicdproj.domain.Product;
import fr.tse.ensias.cicdproj.service.ProductService;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class ProductServiceImpl implements ProductService {
    @Override
    public Product createProduct(Product product) {
        return null;
    }

    @Override
    public Collection<Product> getAllProducts() {
        return null;
    }

    @Override
    public Product getProductByName(String name) {
        return null;
    }

    @Override
    public Product getProductById(Long id) {
        return null;
    }

    @Override
    public Product deleteProduct(Long id) {
        return null;
    }
}
