package fr.tse.ensias.cicdproj.service;

import fr.tse.ensias.cicdproj.domain.Product;

import java.util.Collection;

public interface ProductService {
    Product createProduct(Product product);
    Collection<Product> getAllProducts();
    Product getProductByName(String name);
    Product getProductById(Long id);
    Product deleteProduct(Long id);

}
