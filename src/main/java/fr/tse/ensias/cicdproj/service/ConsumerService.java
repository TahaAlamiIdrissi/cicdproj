package fr.tse.ensias.cicdproj.service;

import fr.tse.ensias.cicdproj.domain.Consumer;
import fr.tse.ensias.cicdproj.domain.Product;

import java.util.Collection;

public interface ConsumerService {
    Product buyProduct(Long productId);
    Consumer getConsumerByUsername(String username);
    Consumer removeConsumer(Long id);
    Collection<Consumer> getAllConsumers();
}
