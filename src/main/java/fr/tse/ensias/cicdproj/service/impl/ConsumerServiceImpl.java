package fr.tse.ensias.cicdproj.service.impl;

import fr.tse.ensias.cicdproj.domain.Consumer;
import fr.tse.ensias.cicdproj.domain.Product;
import fr.tse.ensias.cicdproj.service.ConsumerService;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class ConsumerServiceImpl implements ConsumerService {
    @Override
    public Product buyProduct(Long productId) {
        return null;
    }

    @Override
    public Consumer getConsumerByUsername(String username) {
        return null;
    }

    @Override
    public Consumer removeConsumer(Long id) {
        return null;
    }

    @Override
    public Collection<Consumer> getAllConsumers() {
        return null;
    }
}
